import "./App.css";
import Startup from "./components/Startup";
import DinnerRecipe from "./components/DinnerRecipe";
import React, { Component } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

/// Controller of the components
/// Route path / is the root route, it will be the first component to be rendered
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Startup />} />

        <Route path="/menu" element={<DinnerRecipe />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
