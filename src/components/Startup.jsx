import React, { useEffect, useState } from "react";
import logo from "../../src/svg/logo.svg";
import { useNavigate } from "react-router-dom";
import startStyle from "../styles/startStyle.css";

function Startup() {
  /// done and setDone are here to check if the loading is still running
  const [done, setDone] = useState(undefined);

  /// disable or not the " Let's go" button if the user hasn't wrote
  ///a char* in the input
  const [disabled, isDisabled] = useState(true);

  /// control the opacity of the button, following the disabled hook
  let opacityDict = { opacity: "0.5" };
  const [opacityValue, setOpacity] = useState(opacityDict);

  /// Style the phone on the dimensions of the Iphone 13
  const phoneStyle = {
    position: "absolute",
    width: "390px",
    height: "844px",
    left: "760px",
    top: "0px",
    background: "linear-gradient(163.48deg, #6928B3 11.43%, #E8624D 120.77%)",
  };

  /// Loading of 2 seconds, when done set it to true
  const handeValue = useEffect(() => {
    setTimeout(() => {
      setDone(true);
    }, 2000);
  });

  /// If the input is fullfilled enable the button, else disable it
  const handleInput = (name) => {
    if (name) {
      setOpacity({ opacity: "1" });
      isDisabled(false);
    } else {
      setOpacity({ opacity: "0.5" });
      isDisabled(true);
    }
  };

  /// Route navigator initialisation
  const navigate = useNavigate();

  /// when the user clicks on the "Let's start" or "Skip", go to the
  /// Dinner Recipe (/menu)
  const startOrSkip = (e) => {
    e.preventDefault();
    navigate("/menu");
  };

  return (
    <>
      {!done ? (
        <div>
          <div style={phoneStyle}>
            <img id="startupStyle" src={logo}></img>
          </div>
          <header id="notch"></header>
        </div>
      ) : (
        <div>
          <div style={phoneStyle}>
            <img id="loginStyle" src={logo}></img>

            <h1 id="startText">
              <b>
                Yo 👋<br></br>
                <br></br>What's your<br></br> first name ?
              </b>
            </h1>
            <form id="nameForm">
              <input
                id="nameInput"
                placeholder="Ex : Shazam"
                type="text"
                onChange={(e) => handleInput(e.target.value)}
              ></input>
            </form>
            <div id="choices">
              <div id="skip"></div>
              <button id="skipBtn" onClick={(e) => startOrSkip(e)}>
                Skip
              </button>
              <div id="start"></div>
              <button
                id="startBtn"
                disabled={disabled}
                onClick={(e) => startOrSkip(e)}
                style={opacityValue}
              >
                Let's start{" "}
              </button>
              <p id="arrow">&#8594;</p>
            </div>
          </div>
          <header id="notch"></header>
        </div>
      )}
    </>
  );
}
export default Startup;
