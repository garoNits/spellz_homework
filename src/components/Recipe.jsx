import React, { useEffect, useState } from "react";
import chains from "../../src/svg/chains.svg";

const Recipe = ({ recipes }) => {
  /// simple recipe object, make it a list and  iterate over
  return recipes.map((recipe) => (
    <div id="card">
      <div id="rectangle"></div>
      <img id="chain" src={chains}></img>
      <h6 id="ingredients">{recipe.text}</h6>
    </div>
  ));
};
export default Recipe;
