import React, { useState, useEffect, useRef } from "react";

function RecipeForm(props) {
  const [input, setInput] = useState(props.edit ? props.edit.value : "");
  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

  /// on change, edit the value
  const handleChange = (e) => {
    setInput(e.target.value);
  };

  /// submit the input and reset it
  const handleSubmit = (e) => {
    e.preventDefault();

    props.onSubmit({
      text: input,
    });
    setInput("");
  };

  return (
    <form onSubmit={handleSubmit} className="Recipe-form">
      <input
        placeholder="Add a Recipe"
        value={input}
        onChange={handleChange}
        name="text"
        className="Recipe-input"
        ref={inputRef}
      />
      <button onClick={handleSubmit} className="Recipe-button">
        Add Recipe
      </button>
    </form>
  );
}

export default RecipeForm;
