import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import chains from "../../src/svg/chains.svg";
import MenuStyle from "../styles/MenuStyle.css";
import Recipe from "./Recipe";
import RecipeForm from "./RecipeForm";
import emoji from "../../src/svg/emoji.svg";

function DinnerRecipe() {
  /// step to check where the user is at, if he wants to skip ...
  const [step, setStep] = useState(0);

  /// editing the opacity values of different things
  /// 1) Background
  const [opacityValue, setOpacity] = useState({ opacity: "0.5" });
  /// 2) List of recipes
  const [opacityValueList, setOpacityList] = useState({ opacity: "1" });
  /// 3) Add Item Button
  const [opacityValueItem, setOpacityItem] = useState({ opacity: "0.5" });

  /// Function to add a recipe on click,with a name
  const addRecipe = (recipe) => {
    /// Check if the input is empty or only spaces
    if (!recipe.text || /^\s*$/.test(recipe.text)) {
      return;
    }
    const newRecipes = [recipe, ...recipes];

    setRecipes(newRecipes);
  };

  /// handle the step value based on the choices of the user
  const handleStep = (i) => {
    if (i == 0) {
      setStep(1);
      setOpacityList({ opacity: "0.5" });
      setOpacityItem({ opacity: "1" });
    } else if (i == 2) {
      setStep(3);

      setOpacity({ opacity: "1" });
      setOpacityList({ opacity: "1" });
    } else {
      setStep(3);
      setOpacity({ opacity: "1" });
      setOpacityList({ opacity: "1" });
    }
    console.log(i);
  };
  const [recipes, setRecipes] = useState([]);

  return (
    <div>
      <div id="phone" style={opacityValue}>
        <h1 id="dinnerRecipe">Dinner Recipe</h1>
        <div id="group124">
          <button id="iconButton">?</button>
        </div>
        <h2>Here is a great recipe for your dinner</h2>
      </div>
      <div id="hugeContainer">
        <div class="container" style={opacityValueList}>
          <div id="card">
            <div id="rectangle"></div>
            <img id="chain" src={chains}></img>
            <h6 id="ingredients">Ingredients</h6>
          </div>
          <div id="card">
            <div id="rectangle"></div>
            <img id="chain" src={chains}></img>
            <h6 id="ingredients">Preparation time</h6>
          </div>

          <Recipe recipes={recipes} />
        </div>
        <div id="buttonContainer" style={opacityValueItem}>
          <button id="addItemBtn">
            <div id="rectangle5"></div>
            <h6 id="add">+ Add Item</h6>
          </button>
        </div>

        <RecipeForm onSubmit={addRecipe} />
      </div>
      {step == 0 ? (
        <div id="group158">
          <div id="group154">
            <div id="rectangle2837" />
            <img id="emoji1" src={emoji}></img>
            <h2 id="anyGood">
              Any good recipe starts with good ingredients. Preparation time is
              of the utmost importance
            </h2>
            <h3 id="youCanAdd">
              You can add any item you deem important to share your recipe with
              your community
            </h3>
            <div id="group155">
              <div id="groupButtons">
                <button id="doneBtn" onClick={() => handleStep(1)}>
                  <div id="rectBtn"></div>
                  <p id="textDone">Done !</p>
                </button>
                <button id="addBtn" onClick={() => handleStep(0)}>
                  <div id="rectBtn1"></div>
                  <p id="textAdd">Add an item !</p>
                  <p id="arrow1">&#8594;</p>
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : step == 1 ? (
        <div id="tryIt">
          <img src={emoji} id="emoji2"></img>
          <h2 id="cantThink">
            Can’t thing of anything? How about “cooking time”?
          </h2>
          <button id="tryBtn" onClick={() => handleStep(2)}>
            Try it now !
          </button>
        </div>
      ) : (
        <></>
      )}
      <header id="notch"></header>
    </div>
  );
}

export default DinnerRecipe;
