# The Product Tour Challenge

Homework made in a weekend

The format is the one of an Iphone 13.

To launch the project do :

```
git clone https://gitlab.com/garoNits/spellz_homework.git
```

```
cd spellz_homework && npm install
```

_(You need to have npm installed)_

# Pictures of the project :

!["Loading"](https://cdn.discordapp.com/attachments/509690067044794398/960261700836294696/unknown.png "Loading")
!["Startup"](https://cdn.discordapp.com/attachments/509690067044794398/960262617073582090/unknown.png "Startup")

!["Step0"](https://cdn.discordapp.com/attachments/509690067044794398/960262015748808804/unknown.png "Step0")
!["Step1](https://cdn.discordapp.com/attachments/509690067044794398/960262126679785582/unknown.png "Step1")
!["Step2"](https://cdn.discordapp.com/attachments/509690067044794398/960265123308642394/unknown.png "Step2")

# Issues :

- The Add Item isn't very intuitive and user friendly yet
